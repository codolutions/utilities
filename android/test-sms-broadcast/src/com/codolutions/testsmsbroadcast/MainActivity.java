package com.codolutions.testsmsbroadcast;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.TextView;

public class MainActivity extends Activity
{
    public static final String MESSAGE_RECEIVER_INTENT_FILTER = "android.intent.action.MAIN";
    public static final String BROADCAST_KEY_STATUS_UPDATE = "status_update";

    public static final int EVENT_NONE = 0;
    public static final int EVENT_MESSAGE_RECEIVED = 101;

    public static final String PREF_KEY_PHONE_NUMBER = "number";
    public static final String PREF_KEY_MESSAGE_BODY = "body";

    private BroadcastReceiver messageReceiver;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        updateView();

        IntentFilter intentFilter = new IntentFilter(MESSAGE_RECEIVER_INTENT_FILTER);
        messageReceiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                int what = intent.getIntExtra(BROADCAST_KEY_STATUS_UPDATE, EVENT_NONE);
                if (what != EVENT_MESSAGE_RECEIVED)
                    return;
                updateView();
            }
        };
        registerReceiver(messageReceiver, intentFilter);
    }

    @Override
    protected void onPause()
    {
        if (messageReceiver != null)
            unregisterReceiver(messageReceiver);
        super.onPause();
    }

    public void updateView()
    {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String number = prefs.getString(PREF_KEY_PHONE_NUMBER, null);
        String body = prefs.getString(PREF_KEY_MESSAGE_BODY, null);
        if (number != null && body != null)
        {
            TextView numberView = (TextView) findViewById(R.id.sms_from);
            numberView.setText(number);
            TextView bodyView = (TextView) findViewById(R.id.sms_text);
            bodyView.setText(body);
        }
    }
}
