package com.codolutions.testsmsbroadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

public class IncomingSms extends BroadcastReceiver
{
    final SmsManager sms = SmsManager.getDefault();

    public void onReceive(Context context, Intent intent)
    {
        final Bundle bundle = intent.getExtras();

        try
        {
            if (bundle != null)
            {
                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++)
                {
                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();

                    String senderNum = phoneNumber;
                    String message = currentMessage.getDisplayMessageBody();

                    Log.i("SmsReceiver", "from: " + senderNum + ", message: " + message);

                    int duration = Toast.LENGTH_LONG;
                    Toast toast = Toast.makeText(context, "From: " + senderNum + ", message: " + message, duration);
                    toast.show();

                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
                    final Editor edit = prefs.edit();
                    edit.putString(MainActivity.PREF_KEY_PHONE_NUMBER, senderNum);
                    edit.putString(MainActivity.PREF_KEY_MESSAGE_BODY, message);
                    edit.commit();

                    Intent newIntent = new Intent(MainActivity.MESSAGE_RECEIVER_INTENT_FILTER).putExtra(
                            MainActivity.BROADCAST_KEY_STATUS_UPDATE, MainActivity.EVENT_MESSAGE_RECEIVED);
                    context.sendBroadcast(newIntent);
                }
            }
        }
        catch (Exception e)
        {
            Log.e("SmsReceiver", "Exception receiving SMS: " + e);
        }
    }
}
